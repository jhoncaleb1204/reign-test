# Reign Front End Developer Technical Task

The technical task consisted in a small responsive web app that displays the data extracted using the [Hacker News Api](https://hn.algolia.com/api). In this document it will be explained how I managed to solve the technical task and the process behind doing so.

## Roadmap

In this roadmap section I will specify the steps I followed in order to build this application.

1. Initialize gitlab repository.
2. <details>
    <summary>
    Analysis
    </summary>

   - Check the Zeplin project.
   - Decide what technologies to use based on specifications.
   - Create "Tasks" that will be used to create the issues and branches during development.
   </details>

3. Initialize project with [Vite](https://vitejs.dev/)
4. <details>
    <summary>Configure Jest, commits and setup CI/CD pipeline (Gitlab and Netlify).</summary>
    - The pipeline will run a code quality check and tests on each commit.<br/>
    - The build and deployment stage of the pipeline will only run when changes occur in the main branch (master).
   </details>
5. Create issues and branches for each one, based on the "User Stories" that were created.
6. Mark merge requests as ready when the issue is finished and **merge** them, for the sake of redundancy.
7. Test changes (**this will be done locally and when a change is submitted to gitlab**)
8. If tests are successfull, deploy the changes (**this will be done automatically**)
9. Repeat steps 5,6,7 and 8 until the app is finished.

## Tasks

In this section I will specify the tasks or "stories" that I identified for this project. These will be used for the creation of issues during the development. Here is the list.

1. Add assets like icons, fonts and setup color scheme of app.
2. Create the responsive layout aka the container of all the other components.
3. <details>
     <summary>Integrate API calls to HackerNews</summary>
        This is done before creating the other components in order to have a better understanding of the data structure and types that will be used by the components.
   </details>
4. Create card component
5. Create card skeleton to show while loading
6. Create dropdown component
7. Create tabs component
8. Configure persistence for filters in local storage
9. Configure persistence for favorites in local storage
10. Implement infinite scrolling

## Extra info

Here I am going to write some extra information like the explanation behind some desicions done during the development of this project. Or even some comments that I consider neccessary.

### Vite

I chose to use Vite for the development of this project because of the short ammount of time that was given. The main reason was that vite has some features that can speed up the development workflow, like really fast HMR, pre-bunlding of dependencies and [more](https://vitejs.dev/guide/why.html).

### React

I used React instead of Angular because I have more experience using react and I feel more comfortable developing with that techonology.

### Mobile first design

As requested the app should be responsive and could be used from different mobile devices. That's why I decided to try and implement a mobile-first design in the application. Styling first the components and layout for mobile and modify the style for larger screens.

### Git

I am following [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) specification and using [Husky](https://typicode.github.io/husky/#/) pre-commit hooks to check the formatting of the code before submitting it ([ESlint](https://eslint.org/) and [Prettier](https://prettier.io/)).

### Design tokens

I have used design tokens because I feel that it is useful to define the color of styling that we are going to use for the future components, right from the start. It also makes easy to change the style of the whole app without changing every class of every component.

### State Management

Even though I was tempted to include a state management library like [Redux](https://react-redux.js.org/) or [Mobx](https://mobx.js.org/README.html), I felt that a library like those would be kind of an overkill for the app scope and size. And in order to mantain the bundle size of this project small, I decided to avoid the use of such libraries.
I just managed the states with react hooks in the main component. Although I created a small custom hook to fetch the news in the main component, just to make the code more clean and readable.

### Animations

I also added some css animations to the components just to make the app more visually atractive and improve user experience. Like the animation of the skeleton card that shows when data is loading.
I feel that small things like that can change the way the app is perceived by the user.

### Infinite scroll

I decided to implement the infinite scroll for this app to show new data when the user reaches the bottom of the page. Eventhough I implemented this feature just using react. I believe that in the future it would be useful to use some libraries like react-windowed or react-virtualized. Just to limit the amount of components that are loaded to the ones that are shown. Because having a large amount of components loaded in the app can bring some perforance issues.

### Testing

Testing is an important part of every software develpment cycle. That's why I decided to configure a testing framework([Jest](https://jestjs.io/)). And added the test stage into my continous integration pipeline to test after chages in the submitted code.
Despite that, I ran out of time to implement all of the tests. But I managed to crete a test to check that my "NewsSkeleton" component renders properly.

## Finally

I just wanted to state that the application is deployed using a continous deployent pipeline and netlify on this [site](https://jhonbnites-reign-test.netlify.app/).

If you have any questions or comments, feel free to reach out.

> Jhon Benites
