import { useEffect, useState } from 'react';
import { ModifiedNewsModel } from '../components/NewsContainer/NewsContainer';
import queryOptions from '../constants/dropdown.constants';
import NewsModel from '../models/news/news.model';
import NewsService from '../services/news.service';
import { createUniqueNews, processNews } from './helpers';

export default function useFetchNews(
  page: number,
  query: number,
  filterLoading: boolean,
  favorites: ModifiedNewsModel[],
) {
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [news, setNews] = useState<NewsModel[]>([]);
  const [maxPages, setMaxPages] = useState<number>(1);
  const [newsList, setNewsList] = useState<ModifiedNewsModel[]>([]);
  // To reset news list on query change
  useEffect(() => {
    setNews([]);
  }, [query]);

  // TO fetch news every time page or query changes
  useEffect(() => {
    setIsLoading(true);
    if (filterLoading) {
      setIsLoading(false);
      return;
    }
    if (page > maxPages) {
      setIsLoading(false);
      return;
    }
    NewsService.getNews({ page, query: queryOptions[query]?.value ?? '' }, true)
      .then(({ news: fetchedNews, totalPages }) => {
        setNews(oldNews => createUniqueNews(oldNews, fetchedNews));
        setMaxPages(totalPages);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [page, query]);

  // To process news and change favorites
  useEffect(() => {
    setIsLoading(true);
    processNews(news, favorites)
      .then(result => {
        setNewsList(result);
        setIsLoading(false);
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [news, favorites]);

  return { isLoading, newsList, maxPages };
}
