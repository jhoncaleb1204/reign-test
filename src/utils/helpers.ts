import { ModifiedNewsModel } from '../components/NewsContainer/NewsContainer';
import NewsModel from '../models/news/news.model';

export function truncate(text: string, length: number, suffix: string) {
  if (text && text.length !== 0) {
    const truncated = text.substring(0, length);
    if (text.length > length) {
      return truncated + suffix;
    }
    return truncated;
  }
  return '';
}
export function createUniqueNews(
  oldNews: NewsModel[],
  fetchedNews: NewsModel[],
) {
  const oldArray = [...oldNews];
  const filtered = oldArray.reduce((result: NewsModel[], item: NewsModel) => {
    const isRepeated = fetchedNews.some(
      (newsItem: NewsModel) => newsItem.story_id === item.story_id,
    );
    if (!isRepeated) {
      result.push(item);
    }
    return result;
  }, []);
  return [...filtered, ...fetchedNews];
}
export const processNews = async (
  rawNews: NewsModel[],
  rawFavorites: ModifiedNewsModel[],
): Promise<ModifiedNewsModel[]> => {
  const aux = rawNews.map((item: NewsModel) => {
    const favIndex = rawFavorites.findIndex(
      fav => fav.newsItem.story_id === item.story_id,
    );
    return { newsItem: item, isFav: favIndex !== -1 };
  });
  return Promise.resolve(aux);
};
