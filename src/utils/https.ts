import axios, { AxiosResponse } from 'axios';

const https = axios.create({
  // A good practice is to use environment variables.
  // But since the api url wont change for this app, this is acceptable.
  baseURL: 'https://hn.algolia.com/api/v1',
});

const getData = ({ data }: AxiosResponse) => data;
// Here we are intercepting the reponse to only return the data
// Just to ignore extra information that is not neede for this case
const catchError = async (error: any) => Promise.reject(error);
// Here we can specify an error handler, like refreshing an auth token,
//
https.interceptors.response.use(getData, catchError);
export default https;
