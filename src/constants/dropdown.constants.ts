import Angular from '../../assets/angular.png';
import React from '../../assets/react.png';
import Vue from '../../assets/vue.png';
// Declared here cause, for the scope of the app, the filters won't change
const queryOptions = [
  { text: 'Angular', value: 'angular', img: Angular },
  { text: 'Reacts', value: 'react', img: React },
  { text: 'Vuejs', value: 'vue', img: Vue },
];
export default queryOptions;
