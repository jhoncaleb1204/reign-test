import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { truncate } from '../../utils/helpers';

dayjs.extend(relativeTime);
export interface INewsModel {
  created_at: string;
  author: string;
  story_id: number;
  story_title: string;
  story_url: string;
}
class NewsModel {
  created_at: string;

  author: string;

  story_id: number;

  story_title: string;

  story_url: string;

  constructor({
    created_at,
    author,
    story_id,
    story_title,
    story_url,
  }: INewsModel) {
    this.created_at = dayjs(created_at).fromNow();
    this.story_id = story_id;
    this.author = author;
    // Just making sure titles are not too long.
    // There is no use in saving really long titles if they wont show in component
    this.story_title = truncate(story_title, 220, '...');
    this.story_url = story_url;
  }
}
export default NewsModel;
