import { useEffect, useState } from 'react';
import { NewsCard, NewsSkeleton } from '..';
import NewsModel from '../../models/news/news.model';
import './NewsContainer.css';

export interface ModifiedNewsModel {
  newsItem: NewsModel;
  isFav: boolean;
}
interface INewsContainerProps {
  news: ModifiedNewsModel[];
  loading: boolean;
  onFavorite: (newFav: NewsModel) => void;
}
function NewsContainer({ news, loading, onFavorite }: INewsContainerProps) {
  // State
  const [newsList, setNewsList] = useState<ModifiedNewsModel[]>(news);
  const [isLoading, setIsLoading] = useState<boolean>(loading);

  // Effects
  useEffect(() => {
    setIsLoading(loading);
  }, [loading]);
  useEffect(() => {
    setNewsList(news);
  }, [news]);

  return (
    <div className="news-container">
      {!isLoading && newsList.length === 0 && (
        <p>Sorry, but there are no news to show here ...</p>
      )}
      {newsList.length !== 0 &&
        newsList.map(item => (
          <NewsCard
            key={item.newsItem.story_id}
            newsItem={item.newsItem}
            isFavorite={item.isFav}
            onFavorite={onFavorite}
          />
        ))}
      {isLoading && <NewsSkeleton />}
    </div>
  );
}
export default NewsContainer;
