import './Dropdown.css';
import { useEffect, useRef, useState } from 'react';
import Right from '../../../assets/right.svg';
import Angular from '../../../assets/angular.png';

interface IQueryOption {
  text: string;
  value: string;
  img: string;
}
interface IDropdown {
  options: IQueryOption[];
  handleSelectQuery: (value: number) => void;
  placeholder: string;
  selection: number;
}

function Dropdown({
  options,
  placeholder,
  selection,
  handleSelectQuery,
}: IDropdown) {
  // Refs
  // To track click event when clicked outside dropdown
  const ref = useRef<any>(null);
  // States
  const [isOpen, setIsOpen] = useState<boolean>(false);
  const [selectedIndex, setSelectedIndex] = useState<number>(selection);
  const [optionsList, setOptionsList] = useState<IQueryOption[]>(options);
  // Effects
  // To  handle click outside dropdown
  useEffect(() => {
    const checkIfClickedOutside = (e: any) => {
      // If the menu is open and the clicked target is not within the menu,
      // then close the menu
      if (isOpen && ref.current && !ref.current.contains(e.target)) {
        setIsOpen(false);
      }
    };

    document.addEventListener('mousedown', checkIfClickedOutside);

    return () => {
      // Cleanup the event listener
      document.removeEventListener('mousedown', checkIfClickedOutside);
    };
  }, [isOpen]);
  useEffect(() => {
    setSelectedIndex(selection);
  }, [selection]);

  // Event Handlers
  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };
  const handleSelect = (index: number) => {
    setSelectedIndex(index);
    handleSelectQuery(index);
  };
  return (
    <div ref={ref} className="dropdown" onClick={toggleDropdown}>
      {optionsList ? (
        <>
          <div className="dropdown__header">
            <div className="dropdown-header-title">
              {selectedIndex !== -1 ? (
                <>
                  <img src={optionsList[selectedIndex].img} alt="icon" />
                  {optionsList[selectedIndex].text}
                </>
              ) : (
                placeholder
              )}
            </div>
            <div className={`dropdown__header-icon  ${isOpen ? 'active' : ''}`}>
              <img src={Right} alt="down" />
            </div>
          </div>
          <div className={`dropdown__list ${isOpen ? 'active' : ''}`}>
            {optionsList.map((option: IQueryOption, index) => (
              <div
                key={index}
                className="dropdown__list-item"
                onClick={() => handleSelect(index)}
              >
                <img src={option.img} alt="icon" />
                {option.text}
              </div>
            ))}
          </div>
        </>
      ) : (
        ''
      )}
    </div>
  );
}
export default Dropdown;
