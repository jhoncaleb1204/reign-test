import React, { useEffect, useState } from 'react';
import NewsModel from '../../models/news/news.model';
import './NewsCard.css';
import Time from '../../../assets/time.svg';
import FilledHeart from '../../../assets/filled-heart.svg';
import OutlinedHeart from '../../../assets/outlined-heart.svg';

interface INewsCardProps {
  newsItem: NewsModel;
  isFavorite?: boolean;
  onFavorite: (newFav: NewsModel) => void;
}

function NewsCard({
  newsItem,
  isFavorite = false,
  onFavorite,
}: INewsCardProps) {
  // *******States********
  const [isFav, setIsFav] = useState<boolean>(isFavorite);

  // *******Effects********
  useEffect(() => {
    setIsFav(isFavorite);
  }, [isFavorite]);

  // ******Event Handlers******
  // To handle behaviour of component when is a favorite news
  const toggleFav = (event: React.MouseEvent<HTMLElement>) => {
    event.stopPropagation();
    onFavorite(newsItem);
  };
  const redirectNews = (event: React.MouseEvent<HTMLElement>) => {
    window.open(newsItem.story_url, '_blank', 'noopener,noreferrer');
  };
  return (
    <div className="news-card" onClick={redirectNews}>
      <div className="news-card__info">
        <div className="news-card__date">
          <img src={Time} alt="time" />
          <span>{newsItem.created_at}</span>
        </div>
        <span>{newsItem.story_title}</span>
      </div>
      <div className="news-card__fav">
        {isFav ? (
          <img src={FilledHeart} alt="outlined" onClick={toggleFav} />
        ) : (
          <img src={OutlinedHeart} alt="outlined" onClick={toggleFav} />
        )}
      </div>
    </div>
  );
}
export default NewsCard;
