import { useState } from 'react';
import './Tabs.css';

interface ITabsProps {
  options: string[];
  onSelection: (index: number) => void;
}
function Tabs({ options, onSelection }: ITabsProps) {
  // States
  const [selectedIndex, setSelectedIndex] = useState<number>(0);
  // Event handlers
  const handleClick = (index: number) => {
    setSelectedIndex(index);
    onSelection(index);
  };
  // Hook
  const getSelectedClass = (index: number) => {
    if (index === selectedIndex) {
      return `selected-${index === 0 ? 'left' : 'right'}`;
    }
    return '';
  };

  return (
    <div className="tabs">
      {options.map((option: string, index: number) => (
        <div
          key={option}
          className={`tabs__option ${getSelectedClass(index)}`}
          onClick={() => handleClick(index)}
        >
          {option}
        </div>
      ))}
    </div>
  );
}
export default Tabs;
