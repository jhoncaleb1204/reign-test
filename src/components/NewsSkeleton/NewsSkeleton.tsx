import './NewsSkeleton.css';
import Loading from '../../../assets/loading.svg';

function NewsSkeleton() {
  return (
    <div className="news-skeleton">
      <div className="news-skeleton__info">
        <span className="news-skeleton__line" />
        <span className="news-skeleton__line" />
      </div>
      <div className="news-skeleton__fav">
        <img src={Loading} alt="loading" className="rotate" />
      </div>
    </div>
  );
}
export default NewsSkeleton;
