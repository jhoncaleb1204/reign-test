import { useCallback, useEffect, useRef, useState } from 'react';
import './App.css';
import { Dropdown, NewsContainer, Tabs } from './components';
import NewsModel from './models/news/news.model';
import NewsService from './services/news.service';
import { ModifiedNewsModel } from './components/NewsContainer/NewsContainer';
import queryOptions from './constants/dropdown.constants';
import { TabOptions, tabsOptions } from './constants/tab.constants';
import useFetchNews from './utils/useFetchNews';

function App() {
  // ******States******

  // Filter states
  const [filterLoading, setFilterLoading] = useState<boolean>(true);
  const [query, setQuery] = useState<number>(-1);
  const [page, setPage] = useState<number>(0);

  // Main component states
  const [favorites, setFavorites] = useState<ModifiedNewsModel[]>([]);
  const [tabSelection, setTabSelection] = useState<number>(0);
  const { isLoading, maxPages, newsList } = useFetchNews(
    page,
    query,
    filterLoading,
    favorites,
  );

  // ******** Refs *********
  const observer = useRef<any>();
  // To handle infinite scroll
  const lastNewsElementRef = useCallback(
    node => {
      if (filterLoading) return;
      if (isLoading) return;
      if (observer.current) observer.current.disconnect();
      observer.current = new IntersectionObserver(entries => {
        if (entries[0].isIntersecting && page + 1 <= maxPages) {
          setPage(prevPage => prevPage + 1);
        }
      });
      if (node) observer.current.observe(node);
    },
    [isLoading, filterLoading, maxPages],
  );

  // ******Event handler******
  const onFavorite = (newFav: NewsModel) => {
    const isAlreadyFav = favorites.find(
      fav => fav.newsItem.story_id === newFav.story_id,
    );
    if (isAlreadyFav) {
      setFavorites(favs => {
        const newList = favs.filter(
          fav => fav.newsItem.story_id !== newFav.story_id,
        );
        NewsService.saveFavorites(newList);
        return newList;
      });
    } else {
      setFavorites(favs => {
        const newList = [...favs, { newsItem: newFav, isFav: true }];
        NewsService.saveFavorites(newList);
        return newList;
      });
    }
  };
  const handleQuery = (index: number) => {
    if (index !== query) {
      setQuery(index);
    }
  };
  const handleTabSelect = (index: number) => {
    setTabSelection(index);
  };

  // ******Effects******

  useEffect(() => {
    // To load from storage when is rendered
    setFilterLoading(true);
    NewsService.getFilters().then((storedQuery: string) => {
      if (storedQuery.length !== 0) {
        const foundIndex = queryOptions.findIndex(
          el => el.value === storedQuery,
        );
        setQuery(foundIndex);
      }
    });
    NewsService.getFavorites().then((faves: ModifiedNewsModel[]) => {
      if (faves.length !== 0) {
        setFavorites(faves);
      }
    });
    setFilterLoading(false);
  }, []);

  // ******JSX Component******
  return (
    <div className="App">
      <header className="App-header">
        <h1>HACKER NEWS</h1>
      </header>
      <main>
        <div className="main-content">
          <Tabs options={tabsOptions} onSelection={handleTabSelect} />
          {filterLoading ? (
            <p>Filters are loading ...</p>
          ) : (
            <>
              {tabSelection === 0 ? (
                <Dropdown
                  options={queryOptions}
                  handleSelectQuery={handleQuery}
                  selection={query}
                  placeholder="Select your news"
                />
              ) : (
                <span className="separator" />
              )}
              {tabSelection === TabOptions.ALL ? (
                <NewsContainer
                  news={newsList}
                  loading={isLoading}
                  onFavorite={onFavorite}
                />
              ) : (
                <NewsContainer
                  news={favorites}
                  loading={isLoading}
                  onFavorite={onFavorite}
                />
              )}
              {!isLoading && tabSelection === TabOptions.ALL && (
                <div ref={lastNewsElementRef} className="Pagination" />
              )}
            </>
          )}
        </div>
      </main>
    </div>
  );
}

export default App;
