import { render, screen } from '@testing-library/react';
import { NewsSkeleton } from '../components';

test('Renders Skeleton component correctly', async () => {
  // Setup
  const { container } = await render(<NewsSkeleton />);
  const icon = await screen.findAllByAltText('loading');
  // Checking class of main div
  expect(container.firstChild).toHaveAttribute('class', 'news-skeleton');
  // Check that left div has correct class
  expect(container.firstChild?.firstChild).toHaveAttribute(
    'class',
    'news-skeleton__info',
  );

  // Check that spans have the correct class
  expect(container.firstChild?.firstChild?.firstChild).toHaveAttribute(
    'class',
    'news-skeleton__line',
  );

  // Checking that svg has the correct class
  expect(icon[0]).toHaveAttribute('class', 'rotate');
});
