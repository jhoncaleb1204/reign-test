import { ModifiedNewsModel } from '../components/NewsContainer/NewsContainer';
import NewsModel, { INewsModel } from '../models/news/news.model';
import https from '../utils/https';
import StorageService from './storage.service';

export interface HNSearchParams {
  query: string;
  page: number;
}
const HITS_PER_PAGE = '16';
class NewsService {
  static getNews = async (
    { query, page }: HNSearchParams,
    persistance: boolean = false,
  ): Promise<{ news: NewsModel[]; totalPages: number }> => {
    const params = new URLSearchParams();
    params.append('query', query);
    params.append('page', page.toString());
    // Here we are hardcoding the limit of news that will be returned every request
    params.append('hitsPerPage', HITS_PER_PAGE);
    const raw = (await https.get('/search_by_date', { params })) as any;
    // Here, hits that don't contain all the data we defined inside the model will be skipped.
    const news = raw.hits.reduce((result: NewsModel[], hNew: INewsModel) => {
      if (
        hNew &&
        hNew.author &&
        hNew.created_at &&
        hNew.story_id &&
        hNew.story_title &&
        hNew.story_url
      ) {
        // Just checking if story already exists
        const isRepeated = result.some(
          (newsItem: NewsModel) => newsItem.story_id === hNew.story_id,
        );
        if (!isRepeated) {
          result.push(new NewsModel(hNew));
        }
      }
      return result;
    }, []);
    const totalPages = raw.nbPages ?? 1;
    if (persistance && query && query !== '') {
      this.saveFilters(query);
    }

    return Promise.resolve({ news, totalPages });
  };

  static saveFilters(query: string) {
    // Saving filters in local storage
    StorageService.setStorageValue('filters', JSON.stringify(query));
  }

  static getFilters = async (): Promise<string> => {
    const filter = await StorageService.getStorageValue('filters');
    if (filter) {
      return Promise.resolve(filter.replaceAll('"', ''));
    }
    // Default filters
    return Promise.resolve('');
  };

  static saveFavorites(favorites: ModifiedNewsModel[]) {
    // Storing favorites values into local storage
    StorageService.setStorageValue('faves', JSON.stringify(favorites));
  }

  static getFavorites = async (): Promise<ModifiedNewsModel[]> => {
    const faves = await StorageService.getStorageValue('faves');
    if (faves) {
      return Promise.resolve(JSON.parse(faves));
    }
    // Fallback to default, aka empty favorites
    return Promise.resolve([]);
  };
}
export default NewsService;
