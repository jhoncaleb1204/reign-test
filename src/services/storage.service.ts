// A service to encapsulate the access to local storage

class StorageService {
  static getStorageValue(key: string): Promise<any> {
    return Promise.resolve().then(() => {
      // Getting the stored value
      const saved = localStorage.getItem(key);
      if (saved) return JSON.parse(saved);

      // Fallback when value is not found
      return undefined;
    });
  }

  static setStorageValue(key: string, value: any): Promise<void> {
    return Promise.resolve().then(() => {
      // Storing the value
      localStorage.setItem(key, JSON.stringify(value));
    });
  }
}
export default StorageService;
