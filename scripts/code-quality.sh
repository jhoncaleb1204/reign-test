#!/usr/bin/env sh

export FORCE_COLOR=0

yarn run lint --format=json \
    | head -n -1 \
    | sed 's/^.\+YN[0-9]\+:\W//' \
    | tail -n -1\
    | jq -scM 'flatten(1) | [.[] | .filePath as $filename | .messages[] as $message | {description: $message.message, location: {path: $filename, lines:{begin:$message.line, end:$message.endLine}}}]'